/*
  File name: UsableItem.cpp
  Copyright: (C) 2007 - 2008 Tomasz Kazmierczak

  Creation date: 15.12.2008
  Last modification date: 15.12.2008

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#include "UsableItem.h"

Ammo::Ammo(uint32_t takeAmount) :
	Item(takeAmount),
	mAmount(0)
{
}

void Ammo::setAmount(uint32_t amount)
{
	mAmount = amount;
	if(mAmount > 999)
		mAmount = 999;
}

void Ammo::take()
{
	mAmount += mTakeAmount;
	if(mAmount > 999)
		mAmount = 999;
}

Weapon::Weapon(UsableItemType type, uint16_t bullets, int16_t power, float range, Ammo ammo) :
	UsableItem(type, 1),
	mState(WPNS_NA),
	mBullets(bullets),
	mFireCount(0),
	mPower(power),
	mRange(range),
	mAmmo(ammo)
{
}

void Weapon::setStatus(uint32_t status)
{
	mState = (WeaponState)status;
	mFireCount = 0;
}

void Weapon::damage(uint16_t minshots, float randomnumber)
{
	mFireCount++;
	if(mFireCount > minshots and (mFireCount - minshots) > randomnumber)
		setStatus(WPNS_DAMAGED);
}
