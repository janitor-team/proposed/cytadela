/*
  File name: CCytadelaConfig.h
  Copyright: (C) 2006 - 2013 Tomasz Kazmierczak

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _CCYTADELACONFIG_H_
#define _CCYTADELACONFIG_H_

#include <stdint.h>
#include <stdio.h>

class CCytadelaConfig {
private:
	//settings
	int32_t  mScreenSize[2];
	float    mGameSpeed;
	float    mMouseSpeed;
	char     mLocalization[FILENAME_MAX];
	bool     mDifficult;
	bool     mFullScreen;
	//other variables
	//game speed related
	int32_t  mGameSpeedSetting;
	int32_t  mNumOfGameSpeedSettings;
	float   *mGameSpeedSettings;
	//mouse speed related
	int32_t  mMouseSpeedSetting;
	int32_t  mNumOfMouseSpeedSettings;
	float   *mMouseSpeedSettings;
	//audio
	bool     mAudioStatus;
	//path to sys/ directory
	char     mConfigFilePath[FILENAME_MAX];
	//init flag
	bool     mInitialized;

	CCytadelaConfig() : mInitialized(false) {}
	~CCytadelaConfig();

public:
	static CCytadelaConfig *instance();
	void init(char *sysDirPath);
	
	//get settings
	const int32_t  *getScreenSize() { return mScreenSize; }
	const char *getLocalization() { return mLocalization; }
	bool isDifficult() { return mDifficult; }
	float getGameSpeed() { return mGameSpeed; }
	float getMouseSpeed() { return mMouseSpeed; }
	bool getAudioStatus() { return mAudioStatus; }
	bool isFullScreen() { return mFullScreen; }

	//set settings
	void setScreenSize(int32_t width, int32_t height);
	void setLocalization(char *localization);
	void setDifficult(bool status) { mDifficult = status; }
	bool changeAudioStatus() { mAudioStatus = not mAudioStatus; return mAudioStatus; }
	bool toggleFullScreen() { mFullScreen = not mFullScreen; return mFullScreen; }

	//other functions
	void setNextGameSpeed();
	void setPrevGameSpeed();
	void setNextMouseSpeed();
	void setPrevMouseSpeed();
};

#endif // _CCYTADELACONFIG_H_
