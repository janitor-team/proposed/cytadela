/*
  File name: Localization.h
  Copyright: (C) 2006 - 2013 Tomasz Kazmierczak

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _LOCALIZATION_H_
#define _LOCALIZATION_H_

#include <stdint.h>
#include "CInfoText.h"

#define LOC_NO_ERROR                  0
#define LOC_ERR_CANNOT_OPEN_LOC_FILE  1
#define LOC_ERR_CANNOT_READ_LOC_FILE  2
#define LOC_ERR_OUT_OF_MEMORY         3
#define LOC_ERR_INVALID_ARGUMENT      4

#define CYTADELA_DEFAULT_LOCALIZATION "POLISH"

typedef enum {
	LOC_MENU_START = 0,
	LOC_MENU_LOAD,
	LOC_MENU_OPTIONS,
	LOC_MENU_TRAINING,
	LOC_MENU_INFO,
	LOC_MENU_MENU,
	LOC_MENU_TRAINING_1,
	LOC_MENU_TRAINING_2,
	LOC_MENU_TRAINING_3,
	LOC_MENU_TRAINING_4,
	LOC_MENU_TRAINING_5,
	LOC_MENU_DIFFICULTY_HARD,
	LOC_MENU_DIFFICULTY_EASY,
	LOC_MENU_VIDEO_OPTIONS,
	LOC_MENU_SPEED_SETTINGS,
	LOC_GAME_WELCOME,
	LOC_GAME_PAUSED,
	LOC_GAME_LOCATION,
	LOC_GAME_VISION_NOISE_ON,
	LOC_GAME_VISION_NOISE_OFF,
	LOC_GAME_YOU_ARE_DEAD,
	LOC_GAME_TAKEN_WEAPON_1,
	LOC_GAME_TAKEN_WEAPON_2,
	LOC_GAME_TAKEN_WEAPON_3,
	LOC_GAME_TAKEN_WEAPON_4,
	LOC_GAME_TAKEN_WEAPON_5,
	LOC_GAME_TAKEN_WEAPON_6,
	LOC_GAME_TAKEN_AMMO_1,
	LOC_GAME_TAKEN_AMMO_2,
	LOC_GAME_TAKEN_AMMO_3,
	LOC_GAME_TAKEN_AMMO_4,
	LOC_GAME_TAKEN_AMMO_5,
	LOC_GAME_TAKEN_AMMO_6,
	LOC_GAME_TAKEN_CARD_1,
	LOC_GAME_TAKEN_CARD_2,
	LOC_GAME_TAKEN_CARD_3,
	LOC_GAME_TAKEN_BOMB_1,
	LOC_GAME_TAKEN_BOMB_2,
	LOC_GAME_TAKEN_BOMB_3,
	LOC_GAME_TAKEN_BOMB_4,
	LOC_GAME_TAKEN_BOMB_5,
	LOC_GAME_TAKEN_BOMB_6,
	LOC_GAME_CODE_WEAPONS,
	LOC_GAME_CODE_ENERGY,
	LOC_GAME_CODE_MAP,
	LOC_GAME_WEAPON_DAMAGED,
	LOC_GAME_LOADING_LAUNCHER,
	LOC_GAME_CLOSED_DOOR,
	LOC_GAME_OPEN_DOOR,
	LOC_GAME_OPENING_DOOR,
	LOC_GAME_HOT,
	LOC_SAVE_DUNGEONS_1,
	LOC_SAVE_DUNGEONS_2,
	LOC_SAVE_POWER_PLANT_1,
	LOC_SAVE_POWER_PLANT_2,
	LOC_SAVE_POWER_PLANT_3,
	LOC_SAVE_STORES_1,
	LOC_SAVE_STORES_2,
	LOC_SAVE_STORES_3,
	LOC_SAVE_HANGAR_1,
	LOC_SAVE_HANGAR_2,
	LOC_SAVE_HANGAR_3,
	LOC_SAVE_LABORATORY_1,
	LOC_SAVE_LABORATORY_2,
	LOC_SAVE_LABORATORY_3,
	LOC_SAVE_SEWERS_1,
	LOC_SAVE_SEWERS_2,
	LOC_SAVE_SEWERS_3,
	LOC_SAVE_PRISON_1,
	LOC_SAVE_PRISON_2,
	LOC_SAVE_PRISON_3,
	LOC_SAVE_CENTER_1,
	LOC_SAVE_EMPTY,
	LOC_CPLX_SEWERS,
	LOC_CPLX_LABORATORY,
	LOC_CPLX_POWER_PLANT,
	LOC_CPLX_STORES,
	LOC_CPLX_HANGAR,
	LOC_CPLX_PRISON,
	LOC_CPLX_CENTER,
	LOC_CPLX_STATUS_UNEXPLORED,
	LOC_CPLX_STATUS_NO_ACCESS,
	LOC_CPLX_STATUS_EXPLORED,
	LOC_CPLX_MENU_COMPLEX,
	LOC_CPLX_MENU_STATUS,
	LOC_CPLX_MENU_CHOSEN,
	LOC_CPLX_MENU_NO_ACCESS_YET,
	LOC_CPLX_MENU_ALREADY_EXPLORED,
	LOC_CPLX_MENU_NEED_WHOLE_BOMB,
	LOC_END_LEVEL,
	LOC_END_COMPLETED,
	LOC_END_KILLED,
	LOC_END_TIME,
	LOC_END_BOMB,
	LOC_END_SAVE_QUESTION,
	LOC_END_NO_YES,
	LOC_END_SAVE_CHOOSE_POS,
	LOC_END_SAVE_QUIT,
	LOC_BUMP_WALL,
	LOC_BUMP_DOOR,
	LOC_CARD_INSERT,
	LOC_CARD_WRONG,
	LOC_SLOT_USED,
	LOC_SLOT_BROKEN,
	LOC_NO_SLOT,
	LOC_HAND_NOTHING,
	LOC_HAND_SPRITE,
	LOC_HAND_WALL,
	LOC_HAND_BLOOD,
	LOC_HAND_SLOT,
	LOC_HAND_SLOT_USED,
	LOC_HAND_BLOCADE,
	LOC_HAND_EQUIPMENT,
	LOC_HAND_CORPS,
	LOC_BUTTON,
	LOC_BUMP_OPENING,
	LOC_HAND_ENEMY,
	LOC_HIT,
	LOC_MEDIPACK,
	LOC_POTION,
	LOC_BEER,
	LOC_PAUSE_ON,
	LOC_PAUSE_OFF,
	LOC_NO_CARDS,
	LOC_NO_AMMO,
	LOC_MENU_SPEED_GAME,
	LOC_MENU_SPEED_MOUSE,
	LOC_GAME_GAME_SPEED,
	LOC_GAME_MOUSE_SPEED,
	LOC_YES,
	LOC_NO,
	LOC_MENU_AUDIO_SETTINGS,
	LOC_MENU_FULLSCREEN,
	LOC_MENU_WINDOWED,

	LOCALIZED_TEXTS_COUNT
} LocalizedTexts;

class Localization {
private:
	char  mCurrentLocalization[FILENAME_MAX];
	char *mLocalizationFileBuffer;
	const char *mLocalizedTexts[LOCALIZED_TEXTS_COUNT];
	CInfoText *mInfoTexts;

	bool mLocalizationAvailable[LOCALIZED_TEXTS_COUNT];

	int32_t mError;

	Localization();
	~Localization();
public:

	static Localization *instance();
	void setDefaultLocalization();
	bool loadLocalization(char *localization, CInfoText *infoTexts);
	const char *getText(LocalizedTexts textNum) { return mLocalizedTexts[textNum]; };
	const char *getCurrentLocalization() { return mCurrentLocalization; };

	int32_t getLastError();
};

const char *getLocErrorDesc(int32_t errorNumber);

#endif //_LOCALIZATION_H_
