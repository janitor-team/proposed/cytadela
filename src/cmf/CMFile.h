/*
  File name: CMFile.h
  Copyright: (C) 2008 Tomasz Kazmierczak

  Creation date: 23.04.2007
  Last modification date: 14.12.2008

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _CMFILE_H_
#define _CMFILE_H_

#include <string>
#include "GameObjectsManager.h"

class CMFile {
private:
	FILE   *mFilePtr;
	std::string  m3DgFilePath;

	bool readMapDataSize(uint32_t &size);
	bool readMapData(MapDataPtr &data);
public:
	CMFile() : mFilePtr(0) {};
	~CMFile() { close(); };

	bool open(const char *filename);
	void close();

	bool load(GameObjectsManager *objects);
	bool readInitialPosition(Point3Dg &position);
	bool readInitialDirection(Point3Dg &direction);
	bool readObjectsInfo(GameObjectsManager *objects);
};

#endif //_CMFILE_H_
