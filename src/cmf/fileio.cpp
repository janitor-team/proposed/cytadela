/*
  File name: fileio.cpp
  Copyright: (C) 2009 Tomasz Kazmierczak

  Creation date: 10.06.2009
  Last modification date: 12.06.2009

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#include "fileio.h"

template <>
bool fioWrite<float>(const float &in, FILE *file)
{
	Float32 input = {in};
	return fioWrite(input.i, file);
}

template <>
bool fioWrite<double>(const double &in, FILE *file)
{
	Float64 input = {in};
	return fioWrite(input.i, file);
}

template <>
bool fioRead<float>(float &out, FILE *file)
{
	Float32 output = {out};
	if(not fioRead(output.i, file))
		return false;
	out = output.f;
	return true;
}

template <>
bool fioRead<double>(double &out, FILE *file)
{
	Float64 output = {out};
	if(not fioRead(output.i, file))
		return false;
	out = output.d;
	return true;
}
