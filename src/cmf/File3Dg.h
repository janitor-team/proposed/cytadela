/*
  File name: File3Dg.h
  Copyright: (C) 2007 - 2009 Tomasz Kazmierczak

  Creation date: 20.04.2007
  Last modification date: 13.06.2009

  This file is part of Cytadela

 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.  *

*/

#ifndef _FILE3DG_H_
#define _FILE3DG_H_

#include <stdio.h>
#include "I3Dg.h"

class FileHeader3Dg {
public:
	uint32_t  id;                  //file identifier
	uint32_t  version;             //file version
	uint32_t  reserved1;
	uint32_t  reserved2;
};

class File3Dg : public I3Dg {
private:
	bool read(FILE *file);
	bool readLight(FILE *file);
	bool readTexture(FILE *file);
	bool readMaterial(FILE *file);
	bool readMeshBase(FILE *file);
	bool readMesh(FILE *file);

	bool readColor(ColorRGBA3Dg &color, FILE* file);
	bool readPoint(Point3Dg &point, FILE *file);
	bool readVertex(Vertex3Dg &vertex, FILE *file);
	bool readTriangle(Triangle3Dg &triangle, FILE *file);
	bool readMatrix(Matrix3Dg &matrix, FILE *file);
	bool readAnimation(MeshAnim3Dg &animation, FILE *file);

	//this class only provides additional functionality to the I3Dg interface (no properties),
	//so it should not be instantiated (thus we make the constructor private)
	File3Dg() {};
public:
	bool read(const char *fileName);
};

#endif //_FILE3DG_H_
