/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef CYTADELA_PRIVATE_H
#define CYTADELA_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"1.1.0.0"
#define VER_MAJOR	1
#define VER_MINOR	1
#define VER_RELEASE	0
#define VER_BUILD	0
#define COMPANY_NAME	""
#define FILE_VERSION	"1.1.0"
#define FILE_DESCRIPTION	"Cytadela - a conversion of an Amiga game"
#define INTERNAL_NAME	"cytadela"
#define LEGAL_COPYRIGHT	"(C) 2003-2013 Tomasz Kazmierczak, (C) 2006 Kamil Pawlowski, (C) 2009 Marcin Sekalski, (C) 2009-2010 Tomasz Wisniewski"
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	"cytadela.exe"
#define PRODUCT_NAME	"cytadela"
#define PRODUCT_VERSION	"1.1.0"

#endif /*CYTADELA_PRIVATE_H*/
